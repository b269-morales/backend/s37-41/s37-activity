const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const courseController = require("../controllers/courseController.js");

module.exports = router;