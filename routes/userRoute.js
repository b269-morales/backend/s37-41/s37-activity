const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

module.exports = router;